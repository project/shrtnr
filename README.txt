The shrtnr module works by extending the Redirect module 
(drupal.org/project/redirect) to allow for automatic generation of a shortened 
URL ala services like bit.ly or t.co.

A simple, lightweight module that will be further extended in the future to add 
capabilities including adding campaign tracking tags to shortened URLs for 
better tracking in Google Analytics.

To use, just install Redirect and enable this module.  A checkbox will appear 
when creating URL Redirects to "Automatically generate shortened URL"  By 
enabling this checkbox a random 5 character alphanumberic URL will be created 
to redirect to the path of your choice.
